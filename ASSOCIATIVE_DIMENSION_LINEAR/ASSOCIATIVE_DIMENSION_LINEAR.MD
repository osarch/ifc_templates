```
concept {
IfcElement -> IfcRelConnectsElements:RelatedElement
IfcRelConnectsElements:ConnectionGeometry -> IfcConnectionPointGeometry
IfcConnectionPointGeometry:PointOnRelatingElement -> IfcCartensianPoint_0
IfcCartensianPoint_0  -> "The 1st Point on the Dimension String"
IfcConnectionPointGeometry:PointOnRelatingElement -> IfcCartensianPoint_3
IfcCartensianPoint_3  -> "The 4th Point on the Dimension String"
IfcConnectionPointGeometry:PointOnRelatedElement -> IfcCartensianPoint_4
IfcCartensianPoint_4  -> "Not sure how to 'get' these cartesian points from the related object"
IfcConnectionPointGeometry:PointOnRelatedElement -> IfcCartensianPoint_5
IfcCartensianPoint_5  -> "Not sure how to 'get' these cartesian points from the related object"
IfcRelConnectsElements:RelatingElement -> IfcAnnotation
IfcAnnotation:ObjectType  -> IfcLabel_1
IfcLabel_1 -> constraint_1
constraint_1[label="ASSOCIATIVE_DIMENSION_LINEAR"]
IfcAnnotation:ObjectPlacement  -> IfcObjectPlacement
IfcAnnotation:Representation -> IfcProductDefinitionShape
IfcProductDefinitionShape:Representations -> IfcShapeRepresentation
IfcShapeRepresentation:ContextOfItems -> IfcGeometricRepresentationSubContext
IfcShapeRepresentation:RepresentationIdentifier-> IfcLabel_2
IfcLabel_2 -> constraint_2
constraint_2[label="Annotation"]
IfcShapeRepresentation:RepresentationType ->  IfcLabel_3
IfcLabel_3 -> constraint_3
constraint_3[label="Annotation2D"]
IfcShapeRepresentation:Items -> IfcGeometricCurveSet
IfcShapeRepresentation:Items -> IfcTextLiteral
IfcGeometricRepresentationSubContext:ContextIdentifier ->   IfcLabel_4
IfcLabel_4 -> constraint_4
constraint_4[label="Annotation"]
IfcGeometricCurveSet:Elements -> IfcPolyline
IfcPolyline:Points -> IfcCartensianPoint_0
IfcPolyline:Points -> IfcCartensianPoint_1
IfcCartensianPoint_1 -> "The 2nd 'Offset' Point on the Dimension String"
IfcPolyline:Points -> IfcCartensianPoint_2
IfcCartensianPoint_2 -> "The 3rd 'Offset' Point on the Dimension String"
IfcPolyline:Points -> IfcCartensianPoint_3
IfcTextLiteral:Literal -> IfcPresentableText
IfcPresentableText -> "example 2: {{some text <>}} = some text 1'-5 3/4\""
IfcPresentableText -> "example 3: {{<> some text}} = 1'-5 3/4\" some text"
IfcPresentableText -> "example 4: {{some text}} = some text"
IfcPresentableText -> "example 1: {{<>}} = 1'-5 3/4\""
IfcTextLiteral:Placement -> IfcAxis2Placement2D
IfcTextLiteral:Path -> IfcTextPath
IfcTextLiteral:StyledByItem -> IfcStyledItem_2:Item
IfcStyledItem_2:Styles -> IfcTextStyle
}
```
